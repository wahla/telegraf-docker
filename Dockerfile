FROM alpine:latest
RUN apk update
RUN apk upgrade
RUN apk add libc6-compat

COPY ./buildcontainer/telegraf/telegraf /usr/bin/telegraf

ENTRYPOINT ["/usr/bin/telegraf", "--config", "/etc/telegraf/telegraf.conf"]
